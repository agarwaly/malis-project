

import numpy as np
import pandas as pd
from pathlib import Path
from IPython.display import Markdown, display
import cv2
from matplotlib import pyplot as plt
from sklearn.decomposition import PCA
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report, ConfusionMatrixDisplay
from sklearn.model_selection import cross_val_score, train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler, MinMaxScaler
import pickle
import seaborn as sns
from sklearn.svm import SVC


def printmd(string):
    # Print with Markdowns
    display(Markdown(string))

def print_score(clf, X_train, y_train, X_test, y_test,label_unique):
    f = open("result.txt", 'w')
    pred = clf.predict(X_test)
    clf = classification_report(y_test, pred, output_dict=True)
    print(clf)
    clf_report = pd.DataFrame(clf)
    compression_opts = dict(method='zip', archive_name='out.csv')
    clf_report.to_csv('out.zip', index=False, compression=compression_opts)
    matrix_confusion = confusion_matrix(X_test, pred)
    sns.heatmap(matrix_confusion, square=True, annot=True, cmap='Blues', fmt='d', cbar=False)

    print("Test Result:n================================================")
    print(f"Accuracy Score: {accuracy_score(y_test, pred) * 100:.2f}%")
    print("_______________________________________________")
    print(f"CLASSIFICATION REPORT:n{clf_report}")





def proc_img(filepath):
    """ Create a DataFrame with the filepath and the labels of the pictures
    """
    labels = []

    for it in range(len(filepath)):
        print(it)
        labels += [str(filepath[it]).split("\\")[-2]]



    filepath = pd.Series(filepath, name='Filepath').astype(str)
    labels = pd.Series(labels, name='Label')

    # Concatenate filepaths and labels
    df = pd.concat([filepath, labels], axis=1)

    # Shuffle the DataFrame and reset index
    df = df.sample(frac=1, random_state=0).reset_index(drop=True)

    return df



if __name__ == '__main__':

    dir_ = Path('C:/Users/louis/PycharmProjects/pythonProject/archive/asl_alphabet_train')
    file_paths = list(dir_.glob(r'**/*.jpg'))
    df = proc_img(file_paths)

    print(f'Number of pictures in the dataset: {df.shape[0]}\n')
    print(f'Number of different labels: {len(df.Label.unique())}\n')
    print(f'Labels: {df.Label.unique()}')

    label_unique = df.Label.unique()

    image = cv2.imread(df.Filepath[80000])
    image_base = cv2.imread(df.Filepath[80000], cv2.IMREAD_GRAYSCALE)

    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    blur = cv2.GaussianBlur(gray, (5, 5), 2)

    image = cv2.GaussianBlur(gray, (5, 5), 2)

    thresh1 = cv2.adaptiveThreshold(blur,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY_INV,11,2)
    ret, thresh1 = cv2.threshold(thresh1 , 70, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # percent by which the image is resized
    scale_percent = 50

    # calculate the 50 percent of original dimensions
    width = int(thresh1.shape[1] * scale_percent / 100)
    height = int(thresh1.shape[0] * scale_percent / 100)

    # dsize
    dsize = (width, height)

    # resize image
    thresh1 = cv2.resize(thresh1, dsize)

    img_data_3d = np.asarray(thresh1)
    print(img_data_3d.shape)


    ret, image2 = cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

    plt.subplot(2, 2, 1), plt.imshow(image_base, 'gray')
    plt.subplot(2, 2, 3), plt.imshow(thresh1, 'gray')
    plt.show()

    print(df.Label[1899])
    matrix = np.ndarray((len(df), 10000))
    print(matrix.shape)
    for i in range(len(df)):
        img = cv2.imread(df.Filepath[i])
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        blur = cv2.GaussianBlur(gray, (5, 5), 2)



        th3 = cv2.adaptiveThreshold(blur, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 2)
        ret, img = cv2.threshold(th3, 70, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

        # percent by which the image is resized
        scale_percent = 50

        # calculate the 50 percent of original dimensions
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)

        # dsize
        dsize = (width, height)

        # resize image
        img = cv2.resize(img, dsize)

        img_data_3d = np.asarray(img)
        matrix[i] = img_data_3d.ravel()



    x_train, x_tmp, y_train, y_tmp = train_test_split( matrix, df.Label, test_size=0.3, random_state=42)
    x_validate, x_test, y_validate, y_test = train_test_split(x_tmp, y_tmp, test_size=0.2, random_state=42)
    pca = PCA(n_components=0.95)
    print("pca done")
    scaler = StandardScaler()
    X_train = pca.fit_transform(x_train)
    X_test = pca.transform(x_test)
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    param_grid = {'C': [1,100,1000],
                 'gamma': [10],
                'kernel': ['linear']}
    grid = GridSearchCV(SVC(), param_grid, refit=True, verbose=3, cv=3, n_jobs=-1)
    grid.fit(X_train, y_train)
    best_params = grid.best_params_
    print(f"Best params: {best_params}")
    svm_clf = SVC(**best_params)
    svm_clf.fit(X_train, y_train)
    filename = 'finalized_model.sav'
    pickle.dump(svm_clf, open(filename, 'wb'))
    print_score(svm_clf, X_train, y_train, X_test, y_test, label_unique)



